<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    protected $primaryKey = 'hospital_id';
    protected $fillable = [
        'hospital_name',
        'hospital_detail',
        'phone_number',
        'email',
        'password',
        'address',
        'image'
    ];
}
