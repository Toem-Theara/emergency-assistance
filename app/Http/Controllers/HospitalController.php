<?php
namespace App\Http\Controllers;
use App\Hospital;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hospital = Hospital::paginate(5);
        return view('adminFrontEnd.hospitals.showHospital')->with('hospital',$hospital);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return   view('adminFrontEnd.hospitals.addHospital');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hospital = new Hospital();
        $hospital->hospital_name = $request->input('hospital_name');
        $hospital->firstphone_number = $request->input('firstphone_number');
        $hospital->secondphone_number = $request->input('secondphone_number');
        $hospital->email = $request->input('email');
        $hospital->address = $request->input('hospital_address');
        $hospital->hospital_detail = $request->input('hospital_detail');

        if($request->hasFile('image')){
            $fileNameWithExt = $request->file('image')->getClientOriginalName();

            $filename = pathinfo($fileNameWithExt,PATHINFO_FILENAME);

            $extention = $request->file('image')->getClientOriginalExtension();

            $fileNameToStore = $filename.'-'.time().'.'.$extention;

            $path = $request->file('image')->storeAs('public/hospital_images',$fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
            $hospital->image = $fileNameToStore ;
            $hospital->save();
            return redirect(route('showHospital'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($hospital_id)
    {
        $hospital = Hospital::find($hospital_id);
        return view('frontEnd.hospitalDetail')->with('hospital ', $hospital) ;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
