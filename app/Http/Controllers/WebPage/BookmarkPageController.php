<?php

namespace App\Http\Controllers\WebPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookmarkPageController extends Controller
{
    public function BookmarkPage(){
        return view('frontEnd.bookmark');
    }
}
