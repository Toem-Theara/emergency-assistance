<?php

namespace App\Http\Controllers\WebPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Hospital;

class HomePageController extends Controller
{
    public function HomePage(){
        $hospitals = Hospital::paginate(5);
        return view('frontEnd.home')->with('hospitals',$hospitals);
    }
}
