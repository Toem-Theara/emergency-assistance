<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Emergency-Admin</title>
    <!-- Favicons -->
    <link href={{asset("admin-theme/img/favicon.png")}} rel="icon">
    <link href={{asset("admin-theme/img/apple-touch-icon.png")}} rel="apple-touch-icon">

    <!-- Bootstrap core CSS -->
    <link href={{asset("admin-theme/lib/bootstrap/css/bootstrap.min.css")}} rel="stylesheet">
    <!--external css-->
    <link href={{asset("admin-theme/lib/font-awesome/css/font-awesome.css")}} rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href={{asset("admin-theme/css/zabuto_calendar.css")}}>
    <link rel="stylesheet" type="text/css" href={{asset("admin-theme/lib/gritter/css/jquery.gritter.css")}} />
    <!-- Custom styles for this template -->
    <link href={{asset("admin-theme/css/style.css")}} rel="stylesheet">
    <link href={{asset("admin-theme/css/style-responsive.css")}} rel="stylesheet">


</head>
<body>

        @include('adminFrontEnd.header')
        @include('adminFrontEnd.slideBar')
        <div class="container">
            @yield('content')

        </div>
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="admin-theme/lib/jquery/jquery.min.js"></script>
        <script src="admin-theme/lib/bootstrap/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="admin-theme/lib/jquery.dcjqaccordion.2.7.js"></script>
        <script src="admin-theme/lib/jquery.scrollTo.min.js"></script>
        <script src="admin-theme/lib/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="admin-theme/lib/jquery.sparkline.js"></script>
        <!--common script for all pages-->
        <script src="admin-theme/lib/common-scripts.js"></script>
        <script type="text/javascript" src="admin-theme/lib/gritter/js/jquery.gritter.js"></script>
        <script type="text/javascript" src="admin-theme/lib/gritter-conf.js"></script>
        <!--script for this page-->
        <script src="admin-theme/lib/sparkline-chart.js"></script>
        <script src="admin-theme/lib/zabuto_calendar.js"></script>


</body>
</html>
