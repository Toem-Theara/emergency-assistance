@extends('adminFrontEnd.hospitals.layouts')

@section('content')
<div class="container" style="margin-left:100px; margin-top:80px;">
        <div><h1>Add Hospital</h1></div>
        <form action="{{route('hospitalStore')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-md-6 ">
            <label for="hospital">Hospital Name</label>
            <input type="text" class="form-control" name = "hospital_name" aria-describedby="emailHelp" placeholder="Enter Hospital Name">
        </div>
        <div class="form-group col-md-6">
                <label for="phone">Phone number</label>
                <input type="number" class="form-control" name = "firstphone_number" aria-describedby="emailHelp" placeholder="Enter phone number">
            </div>
            <div class="form-group col-md-6">
                <label for="phone">Phone number</label>
                <input type="number" class="form-control" name = "secondphone_number" aria-describedby="emailHelp" placeholder="Enter phone number">
            </div>
            <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="email" class="form-control" name = "email" aria-describedby="emailHelp" placeholder="Enter Email">
        </div>
        <div class="form-group col-md-6">
                <label for="address">Hospital Address</label>
                <input type="text" class="form-control" name="hospital_address" aria-describedby="emailHelp" placeholder="Enter Hospital Address">
        </div>
        <div class="form-group col-md-12">
                <label for="description">Description</label>
                <textarea class="form-control" name = "hospital_detail" rows="3"></textarea>
        </div>
        <div class="form-group row-md-6" style="margin-left:15px;">
                <label for="image">Add image</label>
                <input type="file" class="form-control-file" name="image">
                </div>
        <button type="submit" class="btn btn-primary" style="margin-left:15px;" href="{{(route('showHospital'))}}">Add</button>
        </form>

</div>

@endsection
