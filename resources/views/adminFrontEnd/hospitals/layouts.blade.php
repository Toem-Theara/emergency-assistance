<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Emergency-Admin</title>
    <!-- Favicons -->
    <link href={{asset("admin-theme/img/favicon.png")}} rel="icon">
    <link href={{asset("admin-theme/img/apple-touch-icon.png")}} rel="apple-touch-icon">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href={{asset("admin-theme/lib/bootstrap/css/bootstrap.min.css")}} rel="stylesheet">
    <!--external css-->
    <link href={{asset("admin-theme/lib/font-awesome/css/font-awesome.css")}} rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href={{asset("admin-theme/css/zabuto_calendar.css")}}>
    <link rel="stylesheet" type="text/css" href={{asset("admin-theme/lib/gritter/css/jquery.gritter.css")}} />
    <!-- Custom styles for this template -->
    <link href={{asset("admin-theme/css/style.css")}} rel="stylesheet">
    <link href={{asset("admin-theme/css/style-responsive.css")}} rel="stylesheet">
    <script src="admin-theme/lib/chart-master/Chart.js"></script>

</head>
<body>

        @include('adminFrontEnd.header')
        @include('adminFrontEnd.slideBar')
        <div class="container">
            @yield('content')

        </div>
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="admin-theme/lib/jquery/jquery.min.js"></script>
        <script src="admin-theme/lib/bootstrap/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="admin-theme/lib/jquery.dcjqaccordion.2.7.js"></script>
        <script src="admin-theme/lib/jquery.scrollTo.min.js"></script>
        <script src="admin-theme/lib/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="admin-theme/lib/jquery.sparkline.js"></script>
        <!--common script for all pages-->
        <script src="admin-theme/lib/common-scripts.js"></script>
        <script type="text/javascript" src="admin-theme/lib/gritter/js/jquery.gritter.js"></script>
        <script type="text/javascript" src="admin-theme/lib/gritter-conf.js"></script>
        <!--script for this page-->
        <script src="admin-theme/lib/sparkline-chart.js"></script>
        <script src="admin-theme/lib/zabuto_calendar.js"></script>
        <script type="text/javascript">
          $(document).ready(function() {
            var unique_id = $.gritter.add({
              // (string | mandatory) the heading of the notification
              title: 'Welcome to Dashio!',
              // (string | mandatory) the text inside the notification
              text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo.',
              // (string | optional) the image to display on the left
              image: 'img/ui-sam.jpg',
              // (bool | optional) if you want it to fade out on its own or just sit there
              sticky: false,
              // (int | optional) the time you want it to be alive for before fading out
              time: 8000,
              // (string | optional) the class name you want to apply to that specific message
              class_name: 'my-sticky-class'
            });

            return false;
          });
        </script>
        <script type="application/javascript">
          $(document).ready(function() {
            $("#date-popover").popover({
              html: true,
              trigger: "manual"
            });
            $("#date-popover").hide();
            $("#date-popover").click(function(e) {
              $(this).hide();
            });

            $("#my-calendar").zabuto_calendar({
              action: function() {
                return myDateFunction(this.id, false);
              },
              action_nav: function() {
                return myNavFunction(this.id);
              },
              ajax: {
                url: "show_data.php?action=1",
                modal: true
              },
              legend: [{
                  type: "text",
                  label: "Special event",
                  badge: "00"
                },
                {
                  type: "block",
                  label: "Regular event",
                }
              ]
            });
          });

          function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
          }
        </script>

</body>
</html>
