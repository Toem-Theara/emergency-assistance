@extends('adminFrontEnd.hospitals.layouts')
@section('content')
<div class="container" style="margin-top:50px; margin-left:100px;">
    <div><h1>Show Hospital</h1></div>
   <div><h3><a href=""></a></h3></div>
<table class="table table-bordered table-striped" style="text-align:center">
    <tr >
     <th width="5%" style="text-align:center">Id</th>
     <th width="10%" style="text-align:center">Image</th>
     <th width="15%" style="text-align:center">Hospital Name</th>
     <th width="10%" style="text-align:center">First Phone Number</th>
     <th width="10%" style="text-align:center">Second Phone Number</th>
     <th width="15%" style="text-align:center">Email</th>
     <th width="15%" style="text-align:center">Address</th>
     <th width="15%" style="text-align:center">Detail</th>
    </tr>
    @foreach($hospital as $row)
     <tr>
      <td>{{ $row->hospital_id }}</td>
      <td>

          <img src="storage/hospital_images/{{$row->image}}" class="img-thumbnail" width="75" height="100" alt="Image" alt="Image">
    </td>
      <td>{{ $row->hospital_name }}</td>
      <td>{{ $row->firstphone_number }}</td>
      <td>{{ $row->secondphone_number }}</td>
      <td>{{ $row->email}}</td>
      <td>{{ $row->address }}</td>
      <td>{{ $row->hospital_detail }}</td>
     </tr>
    @endforeach
    
   </table>

</div>

@endsection
