<aside>
        <div id="sidebar" class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered"><a href="profile.html"><img src={{asset("admin-theme/img/ui-sam.jpg")}} class="img-circle" width="80"></a></p>
            <h5 class="centered">Admin</h5>
            <li class="sub-menu">
              <a href="javascript:;">
                <img src={{asset("admin-theme/img/logo.png")}} alt="hospital" style="weight:40px; height:40px; margin-bottom:15px;">
                <span><h2>Hospitals</h2></span>
                </a>
              <ul class="sub">
                <li><a href="{{route('showHospital')}}"><h4>Show Hospitals</h4></a></li>
                <li><a href=""><h4>Update Hospitals</h4></a></li>
                <li><a href="{{route('createHospital')}}"><h4>Add Hospitals</h4></a></li>
                <li><a href=""><h4>Delete Hospitals</h4></a></li>

              </ul>
            </li>
            <li class="sub-menu">
              <a href="javascript:;">
                <img src={{asset("admin-theme/img/driver.png")}} alt="ambulance" style="weight:40px; height:40px; margin-bottom:15px;">
                <span><h2>Drivers</h2></span>
                </a>
              <ul class="sub">
                <li><a href=""><h4>Show Drivers</h4></a></li>
                <li><a href=""><h4>Update Drivers</h4></a></li>
                <li><a href=""><h4>Add Drivers</h4></a></li>
                <li><a href=""><h4>Delete Drivers</h4></a></li>
              </ul>
            </li>
            <li class="sub-menu">
              <a href="javascript:;">
                    <img src={{asset("admin-theme/img/user.png")}} alt="user" style="weight:40px; height:40px; margin-bottom:15px;">
                <span><h2>Users</h2></span>
                </a>
              <ul class="sub">
                <li><a href=""><h4>Show Users</h4></a></li>
                <li><a href=""><h4>Update Users</h4></a></li>
                <li><a href=""><h4>Add Users</h4></a></li>
                <li><a href=""><h4>Delete Users</h4></a></li>
              </ul>
            </li>

          </ul>
          <!-- sidebar menu end-->
        </div>
      </aside>
