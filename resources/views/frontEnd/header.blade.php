<!--Home page style-->
<header id="home" class="home">
    <div class="overlay-fluid-block">
        <div class="container text-center">
            <div class="row">
                <div class="home-wrapper">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="home-content">
                            <h1>Emergency Assistance</h1>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                                    <div class="home-contact">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Enter the hospital">
                                            <input type="submit" class="form-control" value="Search">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

