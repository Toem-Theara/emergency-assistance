<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Emergency Assistance</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href={{asset("theme/assets/css/bootstrap.min.css")}}>


        <!--For Plugins external css-->
        <link rel="stylesheet" href={{asset("theme/assets/css/plugins.css")}} />
        <link rel="stylesheet" href={{asset("theme/assets/css/roboto-webfont.css")}} />
        <!--Theme custom css -->
        <link rel="stylesheet" href={{asset("theme/assets/css/style.css")}}>
        <!--Theme Responsive css-->
        <link rel="stylesheet" href={{asset("theme/assets/css/responsive.css")}} />
        <script src="theme/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        @include('frontEnd.navigationBar')
        @include('frontEnd.header')
        <div class="container">
            @yield('content')

        </div>
        @include('frontEnd.footer')
            <script src="theme/assets/js/vendor/jquery-1.11.2.min.js"></script>
            <script src="theme/assets/js/vendor/bootstrap.min.js"></script>
            <script src="theme/assets/js/plugins.js"></script>
            <script src="theme/assets/js/modernizr.js"></script>
            <script src="theme/assets/js/main.js"></script>
        </body>
    </html>
