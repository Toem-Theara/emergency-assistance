    @extends('frontEnd.layout')

        @section('content')
        <section id="business" class="portfolio sections">
                <div class="container">
                    <div class="head_title text-center">
                        <h1> Hospital</h1>
                        <p>The Hospital that aviable for you !</p>
                    </div>
                    @foreach ($hospitals as $hospital)
                    <div class="col">
                        <a href="{{route('detail', ['hospital_id'=>$hospital->hospital_id])}} ">
                        <div class="portfolio-wrapper text-center">
                            <div class="row-md-4 col-sm-12 row-xs-12">
                                <div class="community-edition" style="cursor: pointer;">
                                    <img src="storage/hospital_images/{{$hospital->image}}" alt="Image">
                                    <h2>{{$hospital->hospital_name}}</h2>
                                    <h4>{{$hospital->address}}</h4>
                                </div>
                            </div>
                        </div>
                    </a>
                    </div>
                    @endforeach

                </div>
            </section>

        @endsection
