<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="hospital-theme/http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Royal Hospital</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href={{asset("hospital-theme/docs/css/main.css")}}>
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="index.html">Hospital</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <li class="app-search">
          <input class="app-search__input" type="search" placeholder="Search">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src={{asset("hospital-theme/images/hospital.jpg")}} alt="User Image" style="widght:40px ; height:40px">
        <div>
          <p class="app-sidebar__user-name"><b> Hospital</b></p>
          <p class="app-sidebar__user-designation">Phnom Penh</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item active" href=""><i class="glyphicon glyphicon-bell" style="padding:10px;"></i><span class="app-menu__label">Notifications</span></a></li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="fa fa-ambulance" aria-hidden="true" style="padding:10px;"></i><span class="app-menu__label">Driver</span></a>
        </li>
        <li><a class="app-menu__item" href=""><i class="fa fa-hospital-o" aria-hidden="true" style="padding:10px;"></i><span class="app-menu__label">My profile</span></a></li>
      </ul>
    </aside>

    <!-- Essential javascripts  application to work-->
    <script src="hospital-theme/docs/js/jquery-3.2.1.min.js"></script>
    <script src="hospital-theme/docs/js/popper.min.js"></script>
    <script src="hospital-theme/docs/js/bootstrap.min.js"></script>
    <script src="hospital-theme/docs/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="hospital-theme/docs/js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="hospital-theme/docs/js/plugins/chart.js"></script>
    <script type="text/javascript">

  </body>
</html>

