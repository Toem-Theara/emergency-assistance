<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function ()
{
    return view('frontEnd.layout');
});
 Route::get('/layout' , function()
 {
    return view ('frontEnd.layout');
});
Route::get('/bookmark' , function()
{
    return view('frontEnd.bookmark');
});
Route::get('/hospital', function()
{
    return view ('hospitalFrontEnd.hospital');
});
Route::get('/admin' , function()
{
    return view('adminFrontEnd.admin');
});

//Route for Hospital
Route::get('/create_hospital','HospitalController@create')->name('createHospital');
Route::get('/show_hospital' ,'HospitalController@index' )->name('showHospital');
Route::post('/add_hospital' , 'HospitalController@store')->name('hospitalStore');
//Rote for Home Page
Route::get('/homePage' , 'WebPage\HomePageController@HomePage')->name('homePage');
//Route for Bookmark Page
Route::get('/bookmark' , 'WebPage\BookmarkPageController@BookmarkPage')->name('bookmark');
//Route for show hospital detail
Route::get('/hospital_detail/{hospital_id}' , 'HospitalController@show')->name('detail');

